<?php
/** @var $posts App\Models\Post */
\Core\View::render('parts/header', ['title' => 'Posts Page']);
?>

    <table class="table table-dark">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Author</th>
            <th scope="col">Title</th>
            <th scope="col">Content</th>
            <th scope="col">Create</th>
        </tr>
        </thead>
        <tbody>

            <?php foreach ($posts as $post): ?>
                <tr>
                    <th scope="row">
                        <a href="/posts/<?= $post->id; ?>"><?= $post->id; ?></a>
                    </th>
                    <td><?= $post->author_id; ?></td>
                    <td><?= $post->title; ?></td>
                    <td><?= $post->content; ?></td>
                    <td><?= $post->create_at; ?></td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>

<?php
\Core\View::render('parts/footer');
