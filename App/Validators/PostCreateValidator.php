<?php

namespace App\Validators;

use App\Models\User;

class PostCreateValidator
{
    protected $errors = [
        'title_error' => 'The name should contain more than 2 letters',
        'content_error' => 'The name should contain more than 2 letters',
    ];

    protected $rules = [
        'title' => '/[A-Za-zА-Яа-я]{2,}/',
        'content' => '/[A-Za-zА-Яа-я]{2,}/',
    ];

    public function validate($fields)
    {
        foreach ($fields as $key => $field) {
            if (preg_match($this->rules[$key], $field)) {
                unset($this->errors["{$key}_error"]);
            }
        }

        return empty($this->errors);
    }


    public function checkEmailOnExists(string $email)
    {
        $result = false;
        if (User::select()->where(['email', '=', $email])) {
            $this->errors = [
                'email_error' => 'User with this email already exists'
            ];
            $result = true;
        }

        return $result;
    }

    public function getErrors()
    {
        return $this->errors;
    }
}