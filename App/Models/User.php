<?php

namespace App\Models;

use Core\Model;

class User extends Model
{
    static protected $tableName = 'users';

    /**
     * @param $password
     * @return string
     */
    public static function getHashPassword($password)
    {
        return password_hash($password, PASSWORD_DEFAULT);
    }
}