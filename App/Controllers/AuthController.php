<?php

namespace App\Controllers;

use App\Helpers\SessionHelper;
use App\Models\User;
use Core\Controller;
use Core\View;

class AuthController extends Controller
{
    public function login()
    {
        View::render('auth/login');
    }

    /**
     * @return void
     * @throws \Exception
     */
    protected function auth()
    {
        $fields = filter_input_array(INPUT_POST, $_POST, 1);
        if(!in_array('email', array_keys($fields)))
            throw new \Exception("Email not found", 403);

        if(!in_array('password', array_keys($fields)))
            throw new \Exception("Password not found", 403);

        $user = User::select()->where(['email', '=', $fields['email']]);
        if(!password_verify($fields['password'], $user->password))
            throw new \Exception("Password not correct", 403);

        SessionHelper::setUserData($user->id, $user->email);
        site_redirect('posts');
    }

    public function register()
    {
        View::render('auth/register');
    }
}