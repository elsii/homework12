<?php

namespace App\Controllers;

use App\Models\Post;
use App\Validators\PostCreateValidator;
use Core\Controller;
use Core\View;

class PostsController extends Controller
{
    protected function index() {
        $posts = Post::all();
        View::render('posts/index', ['posts' => $posts]);
    }

    /**
     * @param int $id
     * @return void
     */
    protected function show(int $id) {
        $post = Post::select()->where(['id', '=', $id]);
        if(!$post)
            throw new \Exception("not found the post", 404);

        View::render('posts/show', ['post' => $post]);
    }

    /**
     * @param int $id
     * @return void
     */
    protected function create() {
        View::render('posts/create');
    }

    /**
     * @return void
     */
    protected function edit() {
        print "edit";
    }

    /**
     * @return void
     */
    protected function store() {
        $fields = filter_input_array(INPUT_POST, $_POST, 1);
        $validator = new PostCreateValidator();

        if ($validator->validate($fields)) {
            $post = Post::create($fields);

            if ($post) {
                site_redirect('/posts');
            }
        }
        $this->data['data'] = $fields;
        $this->data += $validator->getErrors();

        View::render('posts/create', $this->data);
    }

    /**
     * @return void
     */
    protected function update() {
        print "logic with Store";
    }

    /**
     * @return void
     */
    protected function delete() {
        print "logic with Store";
    }
}