<?php


$router->add('', ['controller' => 'HomeController', 'action' => 'index', 'method' => 'GET']);

$router->add('posts', ['controller' => 'PostsController', 'action' => 'index', 'method' => 'GET']);
$router->add('posts/index', ['controller' => 'PostsController', 'action' => 'index', 'method' => 'GET']);
$router->add('posts/store', ['controller' => 'PostsController', 'action' => 'store', 'method' => 'POST']);
$router->add('posts/create', ['controller' => 'PostsController', 'action' => 'create', 'method' => 'GET']);
$router->add('posts/{id:\d+}', ['controller' => 'PostsController', 'action' => 'show', 'method' => 'GET']);
$router->add('posts/{id:\d+}/edit', ['controller' => 'PostsController', 'action' => 'edit', 'method' => 'GET']);
$router->add('posts/{id:\d+}/update', ['controller' => 'PostsController', 'action' => 'update', 'method' => 'POST']);
$router->add('posts/{id:\d+}/delete', ['controller' => 'PostsController', 'action' => 'delete', 'method' => 'POST']);

$router->add('login', ['controller' => 'AuthController', 'action' => 'login', 'method' => 'GET']);
$router->add('auth', ['controller' => 'AuthController', 'action' => 'auth', 'method' => 'POST']);
$router->add('registration', ['controller' => 'AuthController', 'action' => 'register', 'method' => 'GET']);

$router->add('user/store', ['controller' => 'UsersController', 'action' => 'store', 'method' => 'POST']);
$router->add('user/logout', ['controller' => 'UsersController', 'action' => 'logout', 'method' => 'GET']);
