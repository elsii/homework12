<?php

namespace components;

use function Composer\Autoload\includeFile;

class Router
{
    protected $routes;
    protected $params;
    protected $convertsType = [
        'd' => 'int',
        's' => 'string',
    ];

    public function add(string $url, array $params = [])
    {
        $url = preg_replace('/\//', '\\/', $url);
        $url = preg_replace('/\{([a-z]+)\}/', '(?P<\1>[a-z-]+)', $url);
        $url = preg_replace('/\{([a-z]+):([^\}]+)\}/', '(?P<\1>\2)', $url);
        $url = "/^{$url}$/i";

        $this->routes[$url] = $params;
    }

    public function dispatch($url)
    {
        $url = trim($url, '/');
        if($this->match($url))
        {
            if ($_SERVER['REQUEST_METHOD'] !== $this->params['method']) {
                throw new \Exception('HTTP Method is not a valid');
            }
            unset($this->params['method']);

            $controller = $this->convertToStudlyCaps($this->params['controller']);
            unset($this->params['controller']);

            $controller = $this->getNamespace() . $controller;
            if(class_exists($controller))
            {
                $action = $this->convertToCamelCase($this->params['action']);
                unset($this->params['action']);

                if(method_exists($controller, $action))
                {
                    $controller = new $controller;
                    $controller->$action($this->params);
//                    call_user_func_array(
//                        [
//                            $controller,
//                            $action
//                        ],
//                        $this->params
//                    );
                } else {
                    throw new \Exception("Method '{$action}' doesn't exists in {$controller} not found");
                }
            } else {
                throw new \Exception("Controller class {$controller} not found");
            }
        } else {
            throw new \Exception('No route matched', 404);
        }
    }

    protected function match($url)
    {
        foreach ($this->routes as $route => $params) {
            if (preg_match($route, $url, $matches))
            {
                preg_match_all('|\(\?P<[\w]+>\\\\(\w[\+])\)|', $route, $types);

                $step = 0;
                foreach ($matches as $key => $match) {
                    if(!is_string($key))
                        continue;

                    $types[1] = str_replace('+','',$types[1]);
                    settype($match, $this->convertsType[$types[1][$step]]);
                    $params[$key] = $match;
                    $step++;
                }

                $this->params = $params;
                return true;
            }
        }
    }

    /**
     * @param $url
     * @return mixed|string|null
     */
    protected function removeQueryStringVariable($url)
    {
        if($url == '')
            return $url;

        $parts = explode('&', $url, 2);
        if(!$parts)
            return $url;

        $url = str_contains($parts[0], '=') ? $parts[0] : '';
        return $url;
    }

    /**
     * @return string
     */
    protected function getNamespace(): string
    {
        if(array_key_exists('namespace', $this->params))
        {
            unset($this->params['namespace']);
            return CONTROLLER_NAMESPACE . $this->params['namespace'] . '\\';
        }

        return CONTROLLER_NAMESPACE;
    }

    protected function convertToStudlyCaps($name): string
    {
        return str_replace(' ', '', ucwords(str_replace('-', ' ', $name)));
    }

    /**
     * @param $name
     * @return string
     */
    protected function convertToCamelCase($name): string
    {
        return lcfirst($this->convertToStudlyCaps($name));
    }
}