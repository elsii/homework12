<?php

function site_redirect($path = '')
{
    $siteUrl = "http://{$_SERVER['SERVER_NAME']}/{$path}";
    header("Location: " . $siteUrl);
    exit;
}